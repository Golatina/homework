package by.shag.golatina.mapping;

import by.shag.golatina.jpa.model.Actor;
import org.springframework.stereotype.Component;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

@Component
public class ActorMapper {
    private static final String ID = "id";
    private static final String NAME = "name";
    private static final String LAST_NAME = "last_name";
    private static final String DATE_OF_BIRTH = "date_of_birth";
    private static final String DATE_OF_DEATH = "date_of_death";
    private static final String SEX = "sex";
    private static final String COUNTRY = "country";
    private static final String NUMBER_OF_FILMS = "number_of_films";

    public Actor map(ResultSet resultSet) throws SQLException {
        Actor actor = new Actor();
        actor.setId(resultSet.getInt(ID));
        actor.setName(resultSet.getString(NAME));
        actor.setLastName(resultSet.getString(LAST_NAME));
        actor.setDateOfBirth(resultSet.getDate(DATE_OF_BIRTH));
        actor.setDateOfDeath(resultSet.getDate(DATE_OF_DEATH));
        actor.setSex(resultSet.getBoolean(SEX));
        actor.setCountry(resultSet.getString(COUNTRY));
        actor.setNumberOfFilms(resultSet.getInt(NUMBER_OF_FILMS));
        return actor;
    }

    public void populate(Actor model, PreparedStatement statement) throws SQLException {
        statement.setString(1, model.getName());
        statement.setString(2, model.getLastName());
        statement.setDate(3, model.getDateOfBirth());
        statement.setDate(4, model.getDateOfDeath());
        statement.setBoolean(5, model.getSex());
        statement.setInt(6, model.getNumberOfFilms());
        statement.setString(7, model.getCountry());

    }
}
