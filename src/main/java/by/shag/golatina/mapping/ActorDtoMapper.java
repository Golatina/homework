package by.shag.golatina.mapping;

import by.shag.golatina.api.dto.ActorDto;
import by.shag.golatina.jpa.model.Actor;
import org.springframework.stereotype.Component;

@Component
public class ActorDtoMapper {

    public ActorDto map(Actor actor) {
        ActorDto actorDto = new ActorDto();
        actorDto.setId(actor.getId());
        actorDto.setName(actor.getName());
        actorDto.setLastName(actor.getLastName());
        actorDto.setDateOfBirth(actor.getDateOfBirth());
        actorDto.setDateOfDeath(actor.getDateOfDeath());
        actorDto.setCountry(actor.getCountry());
        actorDto.setSex(actor.getSex());
        actorDto.setNumberOfFilms(actor.getNumberOfFilms());
        return actorDto;
    }

    public Actor populate(ActorDto actorDto) {
        Actor actor = new Actor();
        actor.setId(actorDto.getId());
        actor.setName(actorDto.getName());
        actor.setLastName(actorDto.getLastName());
        actor.setDateOfBirth(actorDto.getDateOfBirth());
        actor.setDateOfDeath(actorDto.getDateOfDeath());
        actor.setCountry(actorDto.getCountry());
        actor.setSex(actorDto.getSex());
        actor.setNumberOfFilms(actorDto.getNumberOfFilms());
        return actor;
    }

}
