package by.shag.golatina.service;

import by.shag.golatina.api.dto.ActorDto;
import by.shag.golatina.exception.EntityRepositoryException;
import by.shag.golatina.jpa.repository.ActorRepository;
import by.shag.golatina.jpa.transaction.EntityTransaction;
import by.shag.golatina.mapping.ActorDtoMapper;
import org.springframework.stereotype.Service;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class ActorService {

    private EntityTransaction tx = new EntityTransaction();

    public ActorDto findById(Integer id) {
        ActorRepository actorRepository = new ActorRepository();
        Optional<ActorDto> result = Optional.empty();
        ActorDtoMapper actorDtoMapper = new ActorDtoMapper();
        try {
            tx.initTransaction(actorRepository);
            result = Optional.of(actorDtoMapper.map(actorRepository.findByID(id).get()));
            tx.commit();
        } catch (EntityRepositoryException e) {
            try {
                tx.rollback();
            } catch (EntityRepositoryException er) {
                er.printStackTrace();
            }
            e.printStackTrace();
        } finally {
            try {
                tx.endTransaction();
            } catch (EntityRepositoryException e) {
                e.printStackTrace();
            }
        }
        return result.get();
    }

    public ActorDto findByName(String name, String surname) {
        ActorRepository actorRepository = new ActorRepository();
        Optional<ActorDto> result = Optional.empty();
        ActorDtoMapper actorDtoMapper = new ActorDtoMapper();
        try {
            tx.initTransaction(actorRepository);
            result = Optional.of(actorDtoMapper.map(actorRepository.findByNameAndSurname(name, surname).get()));
            tx.commit();
        } catch (EntityRepositoryException e) {
            try {
                tx.rollback();
            } catch (EntityRepositoryException er) {
                er.printStackTrace();
            }
            e.printStackTrace();
        } finally {
            try {
                tx.endTransaction();
            } catch (EntityRepositoryException e) {
                e.printStackTrace();
            }
        }
        return result.get();
    }

    public List<ActorDto> findByAll() {
        ActorRepository actorRepository = new ActorRepository();
        List<ActorDto> resultList = new ArrayList<>();
        ActorDtoMapper actorDtoMapper = new ActorDtoMapper();
        try {
            tx.initTransaction(actorRepository);
            resultList = actorRepository.findAll().stream()
                    .map(actorDtoMapper::map)
                    .collect(Collectors.toList());
            tx.commit();
        } catch (EntityRepositoryException e) {
            try {
                tx.rollback();
            } catch (EntityRepositoryException er) {
                er.printStackTrace();
            }
            e.printStackTrace();
        } finally {
            try {
                tx.endTransaction();
            } catch (EntityRepositoryException e) {
                e.printStackTrace();
            }
        }
        return resultList;
    }

    public List<ActorDto> findBySex(boolean sex) {
        ActorRepository actorRepository = new ActorRepository();
        List<ActorDto> resultList = new ArrayList<>();
        ActorDtoMapper actorDtoMapper = new ActorDtoMapper();
        try {
            tx.initTransaction(actorRepository);
            resultList = actorRepository.findBySex(sex).stream()
                    .map(actorDtoMapper::map)
                    .collect(Collectors.toList());
            tx.commit();
        } catch (EntityRepositoryException e) {
            try {
                tx.rollback();
            } catch (EntityRepositoryException er) {
                er.printStackTrace();
            }
            e.printStackTrace();
        } finally {
            try {
                tx.endTransaction();
            } catch (EntityRepositoryException e) {
                e.printStackTrace();
            }
        }
        return resultList;
    }

    public List<ActorDto> findByCountry(String country) {
        ActorRepository actorRepository = new ActorRepository();
        List<ActorDto> resultList = new ArrayList<>();
        ActorDtoMapper actorDtoMapper = new ActorDtoMapper();
        try {
            tx.initTransaction(actorRepository);
            resultList = actorRepository.findByCountry(country).stream()
                    .map(actorDtoMapper::map)
                    .collect(Collectors.toList());
            tx.commit();
        } catch (EntityRepositoryException e) {
            try {
                tx.rollback();
            } catch (EntityRepositoryException er) {
                er.printStackTrace();
            }
            e.printStackTrace();
        } finally {
            try {
                tx.endTransaction();
            } catch (EntityRepositoryException e) {
                e.printStackTrace();
            }
        }
        return resultList;
    }

    public List<ActorDto> findDeadActor() {
        ActorRepository actorRepository = new ActorRepository();
        List<ActorDto> resultList = new ArrayList<>();
        ActorDtoMapper actorDtoMapper = new ActorDtoMapper();
        try {
            tx.initTransaction(actorRepository);
            resultList = actorRepository.findDeadActor().stream()
                    .map(actorDtoMapper::map)
                    .collect(Collectors.toList());
            tx.commit();
        } catch (EntityRepositoryException e) {
            try {
                tx.rollback();
            } catch (EntityRepositoryException er) {
                er.printStackTrace();
            }
            e.printStackTrace();
        } finally {
            try {
                tx.endTransaction();
            } catch (EntityRepositoryException e) {
                e.printStackTrace();

            }
        }
        return resultList;
    }

    public List<ActorDto> findTopActors(Integer number) {
        ActorRepository actorRepository = new ActorRepository();
        List<ActorDto> resultList = new ArrayList<>();
        ActorDtoMapper actorDtoMapper = new ActorDtoMapper();
        try {
            tx.initTransaction(actorRepository);
            resultList = actorRepository.findTopActors(number).stream()
                    .map(actorDtoMapper::map)
                    .collect(Collectors.toList());
            tx.commit();
        } catch (EntityRepositoryException e) {
            try {
                tx.rollback();
            } catch (EntityRepositoryException er) {
                e.printStackTrace();
            }
            e.printStackTrace();
        } finally {
            try {
                tx.endTransaction();
            } catch (EntityRepositoryException e) {
                e.printStackTrace();
            }
        }
        return resultList;
    }

    public void deleteByID(Integer id) {
        ActorRepository actorRepository = new ActorRepository();
        try {
            tx.initTransaction(actorRepository);
            actorRepository.deleteByID(id);
            tx.commit();
        } catch (EntityRepositoryException e) {
            try {
                tx.rollback();
            } catch (EntityRepositoryException er) {
                er.printStackTrace();
            }
            e.printStackTrace();
        } finally {
            try {
                tx.endTransaction();
            } catch (EntityRepositoryException e) {
                e.printStackTrace();
            }
        }
    }

    public void updateDeathDateByID(Integer id, Date date) {
        ActorRepository actorRepository = new ActorRepository();
        try {
            tx.initTransaction(actorRepository);
            actorRepository.updateDeathDateByID(id, date);
            tx.commit();
        } catch (EntityRepositoryException e) {
            try {
                tx.rollback();
            } catch (EntityRepositoryException er) {
                er.printStackTrace();
            }
            e.printStackTrace();
        } finally {
            try {
                tx.endTransaction();
            } catch (EntityRepositoryException e) {
                e.printStackTrace();
            }
        }
    }

    public void updateNumberOfFilmsByID(Integer id, Integer number) {
        ActorRepository actorRepository = new ActorRepository();
        try {
            tx.initTransaction(actorRepository);
            actorRepository.updateNumberOfFilmsByID(id, number);
            tx.commit();
        } catch (EntityRepositoryException e) {
            try {
                tx.rollback();
            } catch (EntityRepositoryException er) {
                er.printStackTrace();
            }
            e.printStackTrace();
        } finally {
            try {
                tx.endTransaction();
            } catch (EntityRepositoryException e) {
                e.printStackTrace();
            }
        }
    }

    public void insertActor(ActorDto actor) {
        ActorRepository actorRepository = new ActorRepository();
        ActorDtoMapper actorDtoMapper = new ActorDtoMapper();
        try {
            tx.initTransaction(actorRepository);
            actorRepository.insertActor(actorDtoMapper.populate(actor));
            tx.commit();
        } catch (EntityRepositoryException e) {
            try {
                tx.rollback();
            } catch (EntityRepositoryException er) {
                er.printStackTrace();
            }
            e.printStackTrace();
        } finally {
            try {
                tx.endTransaction();
            } catch (EntityRepositoryException e) {
                e.printStackTrace();
            }
        }
    }

}
