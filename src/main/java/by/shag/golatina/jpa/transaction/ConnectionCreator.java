package by.shag.golatina.jpa.transaction;

import org.springframework.stereotype.Component;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

@Component
public class ConnectionCreator {

    private final static String PATH = "src\\main\\resources\\database.properties";

    private static Connection createConnection(Properties properties, String url) throws SQLException {
        return DriverManager.getConnection(url, properties);
    }

    public static Connection createConnection() {
        Properties properties = new Properties();
        try {
            properties.load(new FileReader(System.getProperty("user.dir") +  File.separator + PATH));
        } catch (IOException e) {
            throw new RuntimeException("File not found");
        }
        String url = (String) properties.get("db.url");
        try {
            return createConnection(properties, url);
        } catch (SQLException e) {
            throw new RuntimeException("Can't create connection");
        }
    }
}
