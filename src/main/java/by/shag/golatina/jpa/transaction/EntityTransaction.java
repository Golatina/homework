package by.shag.golatina.jpa.transaction;

import by.shag.golatina.exception.EntityRepositoryException;
import by.shag.golatina.jpa.repository.ActorRepository;
import org.springframework.stereotype.Component;

import java.sql.Connection;
import java.sql.SQLException;

@Component
public class EntityTransaction {

    private Connection connection;

    public void initTransaction(ActorRepository... repository) throws EntityRepositoryException {
        if (connection == null) {
            connection = ConnectionCreator.createConnection();
        }
        try {
            connection.setAutoCommit(false);
        } catch (SQLException e) {
            throw new EntityRepositoryException("Problem in initTransaction, impossible to disable autocommit", e);
        }
        for (ActorRepository actorRepository : repository) {
            actorRepository.setConnection(connection);
        }
    }

    public void endTransaction() throws EntityRepositoryException {
        try {
            connection.setAutoCommit(true);
        } catch (SQLException e) {
            throw new EntityRepositoryException("Problem in endTransaction, impossible to enable autocommit", e);
        }
    }

    public void commit() throws EntityRepositoryException {
        try {
            connection.commit();
        } catch (SQLException e) {
            throw new EntityRepositoryException(" impossible to commit autocommit", e);
        }
    }

    public void rollback() throws EntityRepositoryException {
        try {
            connection.rollback();
        } catch (SQLException e) {
            throw new EntityRepositoryException(" impossible to rollback autocommit", e);
        }
    }

}

