package by.shag.golatina.jpa.repository;

import by.shag.golatina.jpa.model.Actor;
import by.shag.golatina.mapping.ActorMapper;
import org.springframework.stereotype.Repository;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Repository
public class ActorRepository {

    private static final String ALL_QUERY = "SELECT * FROM actors";
    private static final String QUERY_BY_ID = "SELECT * FROM actors WHERE id = ?";
    private static final String QUERY_BY_NAME = "SELECT * FROM actors WHERE name = ? and last_name = ?";
    private static final String QUERY_BY_SEX = "SELECT * FROM actors WHERE sex = ?";
    private static final String QUERY_BY_COUNTRY = "SELECT * FROM actors WHERE country = ?";
    private static final String QUERY_DIED_ACTOR = "SELECT * FROM actors WHERE date_of_death < CURRENT_DATE";
    private static final String QUERY_TOP_NUMBER_OF_FILMS = "SELECT * FROM actors ORDER BY number_of_films DESC LIMIT ?";
    private static final String INSERT_PS_DIED_ACTOR =
            "INSERT INTO actors (name, last_name, date_of_birth, date_of_death, sex, number_of_films, country) VALUES (?,?,?::date,?::date,?,?::int,?)";
    private static final String DELETE_PS = "DELETE FROM actors WHERE id = ?";
    private static final String UPDATE_PS_DEATH_DATE = "UPDATE actors SET date_of_death = ?::date WHERE id = ?";
    private static final String UPDATE_PS_NUMBER_OF_FILMS = "UPDATE actors SET number_of_films = ?::int WHERE id = ?";

    private Connection connection;

    public void setConnection(Connection connection) {
        this.connection = connection;
    }

    public List<Actor> findTopActors(Integer top) {
        ActorMapper actorMapper = new ActorMapper();
        try (PreparedStatement statement = connection.prepareStatement(QUERY_TOP_NUMBER_OF_FILMS)) {
            statement.setInt(1, top);
            ResultSet rs = statement.executeQuery();
            List<Actor> result = new ArrayList<>();
            while (rs.next()) {
                result.add(actorMapper.map(rs));
            }
            return result;
        } catch (SQLException e) {
            throw new RuntimeException("Exception in ActorRepository -> findTopActors", e);
        }
    }

    public List<Actor> findDeadActor() {
        ActorMapper actorMapper = new ActorMapper();
        try (PreparedStatement statement = connection.prepareStatement(QUERY_DIED_ACTOR)) {
            ResultSet rs = statement.executeQuery();
            List<Actor> result = new ArrayList<>();
            while (rs.next()) {
                result.add(actorMapper.map(rs));
            }
            return result;
        } catch (SQLException e) {
            throw new RuntimeException("Exception in Actor -> findAll message", e);
        }
    }

    public List<Actor> findByCountry(String country) {
        ActorMapper actorMapper = new ActorMapper();
        try (PreparedStatement statement = connection.prepareStatement(QUERY_BY_COUNTRY)) {
            statement.setString(1, country);
            ResultSet rs = statement.executeQuery();
            List<Actor> result = new ArrayList<>();
            while (rs.next()) {
                result.add(actorMapper.map(rs));
            }
            return result;
        } catch (SQLException e) {
            throw new RuntimeException("Exception in ActorRepository -> findByCountry", e);
        }
    }

    public List<Actor> findBySex(boolean sex) {
        ActorMapper actorMapper = new ActorMapper();
        try (PreparedStatement statement = connection.prepareStatement(QUERY_BY_SEX)) {
            statement.setBoolean(1, sex);
            ResultSet rs = statement.executeQuery();
            List<Actor> result = new ArrayList<>();
            while (rs.next()) {
                result.add(actorMapper.map(rs));
            }
            return result;
        } catch (SQLException e) {
            throw new RuntimeException("Exception in ActorRepository -> findBySex", e);
        }
    }

    public Optional<Actor> findByNameAndSurname(String name, String lastname) {
        ActorMapper actorMapper = new ActorMapper();
        Optional<Actor> optionalActor = Optional.empty();
        try (PreparedStatement statement = connection.prepareStatement(QUERY_BY_NAME)) {
            statement.setString(1, name);
            statement.setString(2, lastname);
            ResultSet rs = statement.executeQuery();
            if (rs.next()) {
                optionalActor = Optional.of(actorMapper.map(rs));
            }
            return optionalActor;
        } catch (SQLException e) {
            throw new RuntimeException("Exception in ActorRepository -> findByNameAndSurname", e);
        }
    }

    public Optional<Actor> findByID(Integer id) {
        ActorMapper actorMapper = new ActorMapper();
        Optional<Actor> optionalActor = Optional.empty();
        try (PreparedStatement statement = connection.prepareStatement(QUERY_BY_ID)) {
            statement.setInt(1, id);
            ResultSet rs = statement.executeQuery();
            if (rs.next()) {
                optionalActor = Optional.of(actorMapper.map(rs));
            }
            return optionalActor;
        } catch (SQLException e) {
            throw new RuntimeException("Exception in Actor -> findByID message", e);
        }
    }

    public List<Actor> findAll() {
        ActorMapper actorMapper = new ActorMapper();
        try (PreparedStatement statement = connection.prepareStatement(ALL_QUERY)) {
            ResultSet rs = statement.executeQuery();
            List<Actor> result = new ArrayList<>();
            while (rs.next()) {
                result.add(actorMapper.map(rs));
            }
            return result;
        } catch (SQLException e) {
            throw new RuntimeException("Exception in Actor -> findAll message", e);
        }
    }

    public void deleteByID(Integer id) {
        try (PreparedStatement statement = connection.prepareStatement(DELETE_PS)) {
            statement.setInt(1, id);
            statement.executeUpdate();
        } catch (SQLException e) {
            throw new RuntimeException("Exception in Actor -> deleteByID message", e);
        }
    }

    public void updateDeathDateByID(Integer id, Date date) {
        try (PreparedStatement statement = connection.prepareStatement(UPDATE_PS_DEATH_DATE, Statement.RETURN_GENERATED_KEYS)) {
            statement.setDate(1, date);
            statement.setInt(2, id);
            statement.executeUpdate();
        } catch (SQLException e) {
            throw new RuntimeException("Exception in Actor -> updateDeathDateByID message", e);
        }
    }

    public void updateNumberOfFilmsByID(Integer id, Integer number) {
        try (PreparedStatement statement = connection.prepareStatement(UPDATE_PS_NUMBER_OF_FILMS, Statement.RETURN_GENERATED_KEYS)) {
            statement.setInt(1, number);
            statement.setInt(2, id);
            statement.executeUpdate();
        } catch (SQLException e) {
            throw new RuntimeException("Exception in Actor -> updateNumberOfFilmsByID", e);
        }
    }

    public void insertActor(Actor entity) {
        ActorMapper actorMapper = new ActorMapper();
        try (PreparedStatement statement = connection.prepareStatement(INSERT_PS_DIED_ACTOR, Statement.RETURN_GENERATED_KEYS)) {
            actorMapper.populate(entity, statement);
            statement.executeUpdate();
            ResultSet rs = statement.getGeneratedKeys();
            if (rs.next()) {
                entity.setId(rs.getInt("id"));
            }
        } catch (SQLException e) {
            throw new RuntimeException("Exception in Actor -> insertActor message", e);
        }
    }
}
