package by.shag.golatina.api.dto;

import org.springframework.stereotype.Component;

import java.sql.Date;

@Component
public class ActorDto {

    private Integer id;
    private String name;
    private String lastName;
    private Date dateOfBirth;
    private Date dateOfDeath;
    private Boolean sex;
    private String country;
    private Integer numberOfFilms;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Date getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(java.sql.Date dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public Date getDateOfDeath() {
        return dateOfDeath;
    }

    public void setDateOfDeath(java.sql.Date dateOfDeath) {
        this.dateOfDeath = dateOfDeath;
    }

    public boolean getSex() {
        return sex;
    }

    public void setSex(boolean sex) {
        this.sex = sex;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public Integer getNumberOfFilms() {
        return numberOfFilms;
    }

    public void setNumberOfFilms(Integer numberOfFilms) {
        this.numberOfFilms = numberOfFilms;
    }

}
