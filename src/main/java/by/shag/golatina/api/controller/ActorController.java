package by.shag.golatina.api.controller;

import by.shag.golatina.api.dto.ActorDto;
import by.shag.golatina.service.ActorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
public class ActorController {

    @Autowired
    private ActorService actorService;

    @GetMapping("/startPage")
    public String getStartForm(Model model) {
        return "startPage.html";
    }

    @GetMapping("/findAll")
    public String findAllMapping(Model model) {
        List<ActorDto> all = actorService.findByAll();
        model.addAttribute("actors", all);
        return "actors.html";
    }

    @GetMapping("/findBySex")
    public String findBySex(@RequestParam(name = "sex") boolean sex, Model model) {
        List<ActorDto> all = actorService.findBySex(sex);
        model.addAttribute("actors", all);
        return "actors.html";
    }

    @GetMapping("/findByCountry")
    public String findByCountry(@RequestParam(name = "country") String country, Model model) {
        List<ActorDto> all = actorService.findByCountry(country);
        model.addAttribute("actors", all);
        return "actors.html";
    }

    @GetMapping("/findDeadActor")
    public String findDeadActor(Model model) {
        List<ActorDto> all = actorService.findDeadActor();
        model.addAttribute("actors", all);
        return "actors.html";
    }

    @GetMapping("/findTopActors")
    public String findTopActors(@RequestParam(name = "top") Integer top, Model model) {
        List<ActorDto> all = actorService.findTopActors(top);
        model.addAttribute("actors", all);
        return "actors.html";
    }

    @GetMapping("/findByNameAndSurname")
    public String findByNameAndSurname(@RequestParam(name = "name") String name,
                                       @RequestParam(name = "lastName") String lastname,
                                       Model model) {
        if (name.isEmpty() && lastname.isEmpty()) {
            return findAllMapping(model);
        }
        model.addAttribute("actor", actorService.findByName(name, lastname));
        return "actor.html";
    }

    @GetMapping("/findByID")
    public String findById(@RequestParam(name = "id") Integer id, Model model) {
        model.addAttribute("actor", actorService.findById(id));
        return "actor.html";
    }

    @DeleteMapping("/deleteByID")
    public String deleteByID(@ModelAttribute(name = "id") Integer id) {
        actorService.deleteByID(id);
        return "confirm.html";
    }

    @PostMapping("/deleteByIDPage")
    public String deleteByIDPage(@ModelAttribute(name = "id") Integer id) {
        actorService.deleteByID(id);
        return "confirm.html";
    }

    @PostMapping("/updateDeathDateByID")
    public String updateDeathDateByID(@ModelAttribute(name = "id") Integer id,
                                      @ModelAttribute(name = "dateOfDeath") java.sql.Date dateOfDeath) {
        actorService.updateDeathDateByID(id, dateOfDeath);
        return "confirm.html";
    }

    @PostMapping("/updateNumberOfFilmsByID")
    public String updateNumberOfFilmsByID(@ModelAttribute(name = "id") Integer id,
                                          @ModelAttribute(name = "numberOfFilms") Integer numberOfFilms) {
        actorService.updateNumberOfFilmsByID(id, numberOfFilms);
        return "confirm.html";
    }

    @PostMapping("/insertActor")
    public String insertActor(@ModelAttribute("actor") ActorDto actor) {
        actorService.insertActor(actor);
        return "confirm.html";
    }

    @PostMapping("/insertActorWithoutDateOfDeath")
    public String insertActorWithoutDateOfDeath(@ModelAttribute("actor") ActorDto actor) {
        actorService.insertActor(actor);
        return "confirm.html";
    }

    @GetMapping(value = "/insertActor/createForm")
    public String getCreateForm(Model model) {
        model.addAttribute("actor", new ActorDto());
        return "createForm.html";
    }

    @GetMapping(value = "/deleteByID/delete")
    public String getDeleteForm() {
        return "delete.html";
    }

}
