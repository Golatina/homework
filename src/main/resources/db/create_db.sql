CREATE TABLE ACTORS(
  id serial PRIMARY KEY,
  name varchar(20) NOT NULL, 
  last_name varchar(30) NOT NULL, 
  date_of_birth date NOT NULL,
  date_of_death date DEFAULT DATE('9999-01-01'),
  sex boolean NOT NULL,
  country varchar(20) NOT NULL,
  number_of_films int DEFAULT 0,
  CONSTRAINT CH_date_of_birth CHECK (date_of_birth < CURRENT_DATE),
  CONSTRAINT CH_age CHECK(date_of_death > date_of_birth),
  CONSTRAINT UQ_Actor UNIQUE(name, last_name, date_of_birth)
);