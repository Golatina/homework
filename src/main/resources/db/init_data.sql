INSERT INTO ACTORS (name, last_name, sex, date_of_birth, date_of_death, number_of_films, country)
VALUES
('Дэвид','Боуи',true,'08-01-1947','10-01-2016',263,'GreatBritain'),
('Алан','Рикман',true,'21-02-1946','04-01-2016',121,'GreatBritain'),
('Иэн','Холм',true,'12-09-1931','19-06-2020',160,'GreatBritain'),
('Чарльз','Чаплин',true,'16-04-1889','25-12-1977',252,'GreatBritain'),
('Анатолий','Папанов',true,'31-10-1922','05-08-1987',170,'Russia')
;

INSERT INTO ACTORS (name, last_name, sex, date_of_birth, number_of_films, country)
VALUES
('Джуд','Лоу',true,'29-12-1972',179,'GreatBritain'),
('Бен','Кингсли',true,'31-12-1943',251,'GreatBritain'),
('Эмма','Томпсон',false,'15-04-1959',205,'GreatBritain'),
('Гари','Олдман',true,'21-03-1958',178,'GreatBritain'),
('Колин','Фёрт',true,'10-09-1960',184,'GreatBritain'),
('Стивен','Фрай',true,'24-08-1957',317,'GreatBritain'),
('Хью','Лори',true,'11-06-1959',207,'GreatBritain'),
('Джон','Клиз',true,'27-10-1939',307,'GreatBritain'),
('Майкл','Кейн',true,'14-03-1933',319,'GreatBritain'),
('Хелена','Бонем-Картер',false,'26-05-1966',177,'GreatBritain'),
('Робби','Колтрейн',true,'30-03-1950',147,'GreatBritain'),
('Энтони','Хопкинс',true,'31-12-1937',254,'GreatBritain'),
('Хелен','Хант',false,'15-06-1963',201,'USA'),
('Мэрил','Стрип',false,'22-06-1949',311,'USA'),
('Алиса','Фрейндлих',false,'08-12-1934',93,'Russia')
;