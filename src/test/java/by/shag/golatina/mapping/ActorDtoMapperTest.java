package by.shag.golatina.mapping;

import by.shag.golatina.api.dto.ActorDto;
import by.shag.golatina.jpa.model.Actor;
import org.junit.jupiter.api.Test;

import java.sql.Date;
import java.time.LocalDate;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

class ActorDtoMapperTest {

    private final String NAME = "Asd";
    private final String LASTNAME = "Zxcv";
    private final Date DATE_OF_BIRTH = Date.valueOf(LocalDate.now().minusDays(1));
    private final Date DATE_OF_DEATH = Date.valueOf(LocalDate.now().minusYears(10));
    private final Boolean SEX = true;
    private final String COUNTRY = "QWER";
    private final Integer NUMBER_OF_FILMS = 15;
    private ActorDtoMapper actorDtoMapper = new ActorDtoMapper();

    @Test
    void mapFromActorDto() {
        Actor actor = new Actor();
        actor.setName(NAME);
        actor.setLastName(LASTNAME);
        actor.setCountry(COUNTRY);
        actor.setSex(SEX);
        actor.setDateOfBirth(DATE_OF_BIRTH);
        actor.setDateOfDeath(DATE_OF_DEATH);
        actor.setNumberOfFilms(NUMBER_OF_FILMS);
        ActorDto result = actorDtoMapper.map(actor);
        assertNull(result.getId());
        assertEquals(result.getName(), NAME);
        assertEquals(result.getLastName(), LASTNAME);
        assertEquals(result.getCountry(), COUNTRY);
        assertEquals(result.getSex(), SEX);
        assertEquals(result.getDateOfBirth(), DATE_OF_BIRTH);
        assertEquals(result.getDateOfDeath(), DATE_OF_DEATH);
        assertEquals(result.getNumberOfFilms(), NUMBER_OF_FILMS);
    }

    @Test
    void mapFromActor() {
        ActorDto actorDto = new ActorDto();
        actorDto.setName(NAME);
        actorDto.setLastName(LASTNAME);
        actorDto.setCountry(COUNTRY);
        actorDto.setSex(SEX);
        actorDto.setDateOfBirth(DATE_OF_BIRTH);
        actorDto.setDateOfDeath(DATE_OF_DEATH);
        actorDto.setNumberOfFilms(NUMBER_OF_FILMS);
        Actor result = actorDtoMapper.populate(actorDto);
        assertNull(result.getId());
        assertEquals(result.getName(), NAME);
        assertEquals(result.getLastName(), LASTNAME);
        assertEquals(result.getCountry(), COUNTRY);
        assertEquals(result.getSex(), SEX);
        assertEquals(result.getDateOfBirth(), DATE_OF_BIRTH);
        assertEquals(result.getDateOfDeath(), DATE_OF_DEATH);
        assertEquals(result.getNumberOfFilms(), NUMBER_OF_FILMS);
    }
}